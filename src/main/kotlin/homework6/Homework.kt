package homework6

fun main() {
    Lion("SIMBA", 6, 7).apply {
        eat(Food.MEAT)
        println("Сытость $fullness")
    }
    Shimpanze("Monkey", 3, 4).apply {
        eat(Food.MEAT)
        println("Сытость  $fullness")
    }

}
enum class Food() {
    MEAT, HERB
}

//val List: List<String> = listOf("Банан", "Мясо", "Трава")
//fun AllAnimalsEat(arrayAnimals: List<Animal>, arrayFood: List<String>) {
  //  for (animal in arrayAnimals) {
   //     for (food in arrayFood) {
    //        animal.eat(food)
        //}
  //  }
//}
