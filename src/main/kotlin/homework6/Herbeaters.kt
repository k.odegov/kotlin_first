package homework6

abstract class Herbeaters(name: String, height: Int, weight: Int) : Animal(name, height, weight) {
    override var foodPreference: Array<Food> = arrayOf(Food.HERB)
}