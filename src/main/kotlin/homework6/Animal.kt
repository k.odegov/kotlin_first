package homework6

abstract class Animal(val name: String, var height: Int, var weight: Int) {
    open lateinit var foodPreference: Array<Food>
    open var fullness: Int = 0
    open fun eat(food: Food) {
        if (food in foodPreference) {
            fullness++
        }
    }
}