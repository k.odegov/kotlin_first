package homework4

fun main() {
    val myArray = arrayOf(1, 25, 10, 6, 22, 17, 8, 14, 23)
    var evenNumber: Int = 0
    var oddNumber: Int = 0
    for (item in myArray) {
        if (item % 2 == 0) {
            evenNumber++
        } else {
            oddNumber++
        }
    }
    println("$evenNumber четных монет получит Джо")
    println("$oddNumber нечетных монет получит команда")
}

