package homework4

fun main() {
    val marks = arrayOf(3, 4, 5, 2, 3, 5, 5, 2, 4, 5, 2, 4, 5, 3, 4, 3, 3, 4, 4, 5)
    var fiveRecipient = 0
    var fourRecipient: Int = 0
    var threeRecipient: Int = 0
    var twoRecipient: Int = 0
    for (item in marks) {
        when (item) {
            2 -> twoRecipient++
            3 -> threeRecipient++
            4 -> fourRecipient++
            else -> fiveRecipient++
        }
    }
    val marksCountPercent: Double = 100.0 / (fiveRecipient + fourRecipient + threeRecipient + twoRecipient)
    val fiveRecipientPercent = fiveRecipient * marksCountPercent
    val fourRecipientPercent = fourRecipient * marksCountPercent
    val threeRecipientPercent = threeRecipient * marksCountPercent
    val twoRecipientPercent = twoRecipient * marksCountPercent
    println("Отличников - $fiveRecipientPercent%")
    println("Хорошистов - $fourRecipientPercent%")
    println("Троечников - $threeRecipientPercent%")
    println("Двоечников - $twoRecipientPercent%")
}