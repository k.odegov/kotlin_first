package homework5

abstract class Animal(val name: String, var height: Int, var weight: Int) {
    open lateinit var foodPreference: Array<Food>
    open var fullness: Int = 0
    open fun eat(food: Food) {
        if (food in foodPreference) {
            fullness++
        }
    }
}

abstract class Predator(name: String, height: Int, weight: Int) : Animal(name, height, weight) {
    override var foodPreference: Array<Food> = arrayOf(Food.MEAT)
}

abstract class Herbeaters(name: String, height: Int, weight: Int) : Animal(name, height, weight) {
    override var foodPreference: Array<Food> = arrayOf(Food.HERB)
}

fun main() {
    Lion("SIMBA", 6, 7).apply {
        eat(Food.MEAT)
        println("Сытость $fullness")
    }
    Shimpanze("Monkey", 3, 4).apply {
        eat(Food.MEAT)
        println("Сытость  $fullness")
    }
}
enum class Food() {
    MEAT,
    HERB
}






