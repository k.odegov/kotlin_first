
fun main () {
    val whiskeyOrder: Byte = 50
    val pinacoladaOrder: Short = 200
    val freshOrder: Long = 3000000000L
    val aleOrder: Double = 0.666666667
    val limonadeOrder: Int = 18500
    val colaOrder: Float = 0.5f
    val randomOrder: String = "Что-то авторское!"
    println("Заказ - '$whiskeyOrder мл виски' готов!")
    println("Заказ - '$pinacoladaOrder мл пина колады'готов!")
    println("Заказ - '$freshOrder капель фреша' готов!")
    println("Заказ - '$aleOrder литров эля' готов!")
    println("Заказ - '$limonadeOrder мл лимонада' готов!")
    println("Заказ - '$colaOrder литров колы' готов!")
    println("Заказ - '$randomOrder' готов!")

}
